<?php

namespace Drupal\keyvalue_filestore;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Register arguments for services.
 */
class KeyvalueFilestoreServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $parameter_key  = 'keyvalue_filestore.json_key_value_directory';
    $parameter = $container->getParameterBag()->get($parameter_key);
    if ($value = getenv('JSON_KEY_VALUE_DIRECTORY')) {
      $parameter = $value;
    }
    $container->setParameter($parameter_key, $parameter);
  }

}
