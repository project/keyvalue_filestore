<?php

namespace Drupal\keyvalue_filestore\KeyValueStore;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\KeyValueStore\StorageBase;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Defines a JSON key/value store implementation.
 */
class JsonStorage extends StorageBase {

  /**
   * The PHP serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $phpSerializer;

  /**
   * The JSON serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerializer;

  /**
   * The directory path
   * @var string
   */
  protected $path;

  /**
   * JSON data decoded as an array.
   *
   * @var string[]
   */
  protected $json;

  /**
   * Overrides Drupal\Core\KeyValueStore\StorageBase::__construct().
   *
   * @param string $collection
   *   The name of the collection holding key and value pairs.
   * @param \Drupal\Component\Serialization\SerializationInterface $php_serializer
   *   The PHP serialization class to use.
   * @param \Drupal\Component\Serialization\SerializationInterface $json_serializer
   *   The JSON serialization class to use.
   * @param string $directory_path
   *   The directory path.
   */
  public function __construct(string $collection, SerializationInterface $php_serializer, SerializationInterface $json_serializer, string $directory_path) {
    if (!is_dir($directory_path)) {
      throw new \RuntimeException(sprintf('"%s" does not exist as a directory.', $directory_path));
    }
    if (preg_match('@[^a-z0-9_]+@', $collection)) {
      throw new \RuntimeException(sprintf('Collection name "%s"  must contain only lowercase letters, numbers, and underscores.', $collection));
    }
    parent::__construct($collection);
    $this->phpSerializer = $php_serializer;
    $this->jsonSerializer = $json_serializer;
    $this->path = $directory_path . DIRECTORY_SEPARATOR . $this->collection . '.json';
    $file_system = new Filesystem();
    if (!$file_system->exists($this->path)) {
      $file_system->touch($this->path);
      file_put_contents($this->path, '{}', LOCK_EX);
    }
    // Make sure the file is secure.
    $file_system->chmod($this->path, 0640);
  }

  /**
   * {@inheritdoc}
   */
  public function has($key) {
    return array_key_exists($key, $this->getJsonArray());
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys) {
    $values = [];
    foreach ($keys as $key) {
      if ($this->has($key)) {
        $values[$key] = $this->phpSerializer::decode($this->getJsonArray()[$key]);
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    $json = $this->getJsonArray();
    $values = [];
    foreach ($json as $key => $item) {
      $values[$key] = $this->phpSerializer::decode($item);
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $json = $this->getJsonArray();
    $json[$key] = $this->phpSerializer::encode($value);
    file_put_contents($this->path, $this->jsonSerializer::encode($json), LOCK_EX);
  }

  /**
   * {@inheritdoc}
   */
  public function setIfNotExists($key, $value) {
    if (!$this->has($key)) {
      $this->set($key, $value);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function rename($key, $new_key) {
    if ($value = $this->get($key)) {
      $this->delete($key);
      $this->set($new_key, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    $json = $this->getJsonArray();
    foreach ($keys as $key) {
      unset($json[$key]);
    }
    file_put_contents($this->path, $this->jsonSerializer::encode($json), LOCK_EX);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    file_put_contents($this->path, '{}', LOCK_EX);
  }

  /**
   * Get the JSON data as a PHP array.
   *
   * @return string[]
   */
  protected function getJsonArray(): array {
    if (!isset($this->json)) {
      $file = file_get_contents($this->path);
      $json = $this->jsonSerializer::decode($file);
      // Play it safe and always make sure we are dealing w/ an array, even if
      // it is empty.
      $this->json = $json ?? [];
    }
    return $this->json;
  }

}
