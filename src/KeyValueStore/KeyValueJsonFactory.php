<?php

namespace Drupal\keyvalue_filestore\KeyValueStore;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Defines the key/value store factory for the JSON backend.
 */
class KeyValueJsonFactory implements KeyValueFactoryInterface {

  /**
   * The PHP serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $phpSerializer;

  /**
   * The JSON serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerializer;

  /**
   * The directory path.
   *
   * @var string
   */
  protected $directory_path;

  /**
   * Constructs this factory object.
   *
   * @param \Drupal\Component\Serialization\SerializationInterface $php_serializer
   *   The PHP serialization class to use.
   * @param \Drupal\Component\Serialization\SerializationInterface $json_serializer
   *   The JSON serialization class to use.
   * @param string $directory_path
   *   The directory path.
   */
  public function __construct(SerializationInterface $php_serializer, SerializationInterface $json_serializer, string $directory_path) {
    $this->phpSerializer = $php_serializer;
    $this->jsonSerializer = $json_serializer;
    $this->directory_path = $directory_path;
  }

  /**
   * {@inheritdoc}
   */
  public function get($collection) {
    return new JsonStorage($collection, $this->phpSerializer, $this->jsonSerializer, $this->directory_path);
  }

}
